# frozen_string_literal: true

require 'json'
require 'csv'

# Convert translation files to CSV and back. This module facilitates translation to non programmers.
# It will create a CSV file that can be edited in Excel or GSheets and it will transform it back to
# json.
module CsvParser
  NEWLINE_REPLACEMENT = '0d!vje0w45qw~e'
  NEWLINE = '\n'
  I18N_PATH = File.expand_path(Dir.pwd)

  HEADER_I18N = 'I18N VARIABLE'
  HEADER_TRANSLATION = 'TRANSLATION'

  DEFAULT_LANGUAGE = 'english'

  def self.export_to_csv(language)
    file = File.read(File.join(I18N_PATH, "#{DEFAULT_LANGUAGE}.json"))
    file.gsub!(NEWLINE, NEWLINE_REPLACEMENT)
    english = JSON.parse(file)
    file = File.read(File.join(I18N_PATH, "#{language}.json"))
    file.gsub!(NEWLINE, NEWLINE_REPLACEMENT)
    i18n = JSON.parse(file)
    header_eng = "#{DEFAULT_LANGUAGE.upcase} #{HEADER_TRANSLATION}"
    header_lang = "#{language.upcase} #{HEADER_TRANSLATION}"
    rows = [[HEADER_I18N, header_eng, header_lang]]
    english.each_pair do |i18n_var, eng|
      next if eng.nil?

      eng.gsub!(NEWLINE_REPLACEMENT, NEWLINE) if eng.is_a? String
      translation = i18n[i18n_var]
      translation.gsub!(NEWLINE_REPLACEMENT, NEWLINE) if i18n.is_a? String
      rows << [i18n_var, eng, translation]
    end
    File.write("#{DEFAULT_LANGUAGE}_to_#{language}_export.csv", rows.map(&:to_csv).join)
  end

  def self.parse_to_json(language)
    csv = CSV.read(File.join(I18N_PATH, "#{language}.csv"), headers: true)
    lang = "#{language.upcase} #{HEADER_TRANSLATION}"

    i18n = {}
    csv.each do |row|
      next if row[lang].nil?

      begin
        # Parse nested hashes (eg. units, square untis, primary units, ...)
        i18n[row[HEADER_I18N]] = JSON.parse(row[lang])
      rescue StandardError
        i18n[row[HEADER_I18N]] = row[lang]
      end
    end

    new_file = File.join(I18N_PATH, "#{language}.json")
    File.write(new_file, JSON.pretty_generate(i18n))
  end
end
