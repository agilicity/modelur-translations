# frozen_string_literal: true

require 'json'
require 'digest'
require 'fileutils'
# Different tools to handle translations
# rubocop:disable Metrics/ModuleLength
module TranslationTools
  # Path to where language files are saved
  LANG_PATH = File.expand_path(File.join(File.dirname(__FILE__), '../')).freeze
  OLD_UNUSED_TRANSLATIONS_PATH = File.join(LANG_PATH, 'unused_translations').freeze
  DIGEST_NAME = 'digest.json'
  DIGEST_PATH = File.join(LANG_PATH, DIGEST_NAME).freeze
  MODELUR_PATH = File.join(LANG_PATH, '../', 'Modelur', 'i18n').freeze
  REACT_PATH = File.join(LANG_PATH, '../', '_react-ui', 'src', 'i18n').freeze
  LANGUAGES_NAME = 'languages.json'
  DEFAULT_LANGUAGE = 'english'

  NAME_KEY = 'name'
  TRANSLATION_KEY = 'translation'

  module_function

  # Load language from appropriate JSON filec
  # @param language [String] the name of the language
  # @return [Hash] the translations in a hash
  def load_lang(language)
    path = File.join(LANG_PATH, "#{language}-sl.json")
    JSON.parse(File.read(path, encoding: Encoding::UTF_8))
  end

  # Load language names from `languages.json`
  # @return [Hash{String=>String}] the names of the languages
  # @example
  #   TranslationTools.load_names
  def load_names
    path = File.join(LANG_PATH, LANGUAGES_NAME)
    JSON.parse(File.read(path, encoding: Encoding::UTF_8))
  end

  # Load all languages from JSON files and complete missing translations with english version.
  # @return [Hash] all language translations combined into a single hash
  def load_and_complete_all
    names = load_names
    all = { NAME_KEY => names }
    all[TRANSLATION_KEY] = names.each.collect do |lang, _name|
      lang_hash = load_lang(lang)
      [lang, lang_hash]
    end.to_h
    fallback = all[TRANSLATION_KEY][DEFAULT_LANGUAGE]
    all[TRANSLATION_KEY].each do |lang, hash|
      next if lang == DEFAULT_LANGUAGE

      complete_lang!(hash, fallback)
    end
    all
  end

  # Print usage
  def usage
    puts <<~USAGE
      ruby src/translation_tools.rb --compile
      ruby src/translation_tools.rb --copy-to-modelur
      ruby src/translation_tools.rb --all
      ruby src/translation_tools.rb --remove_unused_translations

    USAGE
  end

  # Remove translations from all i18n-sl.json files.
  # @note running this changes all language files! It also moves any
  #   removed entries to corresponding files in /unused_translations.
  # @example
  #   TranslationTools.remove_all_unused_translations!
  def remove_all_unused_translations!
    unused_keys_file = File.join(File.dirname(__FILE__), 'unused.txt')
    unused_keys = File.readlines(unused_keys_file).collect { |l| [l.chomp, true] }.to_h
    load_names.each_key do |lang|
      remove_unused_translations!(lang, unused_keys)
    end
  end

  # Remove translations from specified i18n-sl.json.
  # @note running this changes specified language file! It also moves any
  #   removed entries to corresponding file in /unused_translations.
  # @param lang [String] language
  # @param unused_keys [Hash] keys that should be removed
  # @param path [String] path to original translations files
  # @param unused_path [String] path to store unused translations
  # @example
  #   TranslationTools.remove_unused_translations('english')
  def remove_unused_translations!(lang, unused_keys, path = LANG_PATH, unused_path = OLD_UNUSED_TRANSLATIONS_PATH)
    language = load_lang(lang)
    used_translations = HashUtils.delete_unused_keys!(language, unused_keys)
    unused_translations = language.select { |k, _v| used_translations[k].nil? }

    # Write clean translations
    content = JSON.pretty_generate(used_translations)
    filename = "#{lang}-sl.json"
    filepath = File.join(path, filename)
    File.write(filepath, content, encoding: Encoding::UTF_8, universal_newline: true)

    # Store removed translations, make sure not to overwrite existing ones
    filepath = File.join(unused_path, filename)
    if File.exist?(filepath)
      previous_old_unused_translations = JSON.parse(File.read(filepath, encoding: Encoding::UTF_8))
      unused_translations.merge!(previous_old_unused_translations)
    end
    content = JSON.pretty_generate(unused_translations)
    File.write(filepath, content, encoding: Encoding::UTF_8, universal_newline: true)
  end

  # Complete missing translations with english text.
  #
  # @param [Hash] translation the translation to complete
  # @param [Hash] fallback the fallback translation
  # @note This method changes passed `translation` object in place and returns nothing.
  def complete_lang!(translation, fallback)
    HashUtils.delete_extra_keys!(translation, fallback)
    HashUtils.update_rec!(translation, fallback)
  end

  def lang_filename(language)
    "#{language}.json"
  end

  # Load and complete translations for all language and save the result into one file for each language.
  # @note this method returns nothing as the results are saved into the file `translations.json`.
  def compile_all_languages(compiled_hash, path = LANG_PATH)
    print "Saving compiled files to #{path}"
    all = compiled_hash
    digest = {}
    all[TRANSLATION_KEY].each do |lang, hash|
      print '.'
      content = JSON.pretty_generate(hash)
      filename = lang_filename(lang)
      filepath = File.join(path, filename)
      File.write(filepath, content, encoding: Encoding::UTF_8, universal_newline: true)
      digest[filename] = {
        'SHA256' => Digest::SHA256.file(filepath).hexdigest
      }
    end
    digest_content = JSON.pretty_generate(digest)
    File.write(File.join(path, DIGEST_NAME), digest_content, encoding: Encoding::UTF_8)
    puts 'COMPLETED'
  end

  def compile
    compile_all_languages(load_and_complete_all, LANG_PATH)
  end

  def copy_to_modelur
    names = load_names
    [MODELUR_PATH, REACT_PATH].each do |path|
      names.each_key do |lang|
        filename = lang_filename(lang)
        puts "Copy #{filename} to #{path}"
        FileUtils.cp(File.join(LANG_PATH, filename), File.join(path, filename))
      end
      puts "Copy #{DIGEST_NAME} to #{path}"
      FileUtils.cp(DIGEST_PATH, File.join(path, DIGEST_NAME))
      puts "Copy #{LANGUAGES_NAME} to #{path}"
      FileUtils.cp(File.join(LANG_PATH, LANGUAGES_NAME), File.join(path))
    end
  end

  # A few utilities to work with recursive hashes
  module HashUtils
    module_function

    # Update only values that are missing or equal to nil.
    # @param [Hash] hash_to_update The hash to be updated
    # @param [Hash] fallback the hash to get missing values from
    # @example
    #   h = {a: 1, b: nil}
    #   update = {a: 2, b: 3, c: 4}
    #   TranslationTools.update_nil!(h, update)
    #   # h => { a:1, b: 3, c: 4}
    # @note This method changes passed `hash` object in place and returns nothing.
    def update_nil!(hash_to_update, fallback)
      return unless hash_to_update.is_a?(Hash)
      return if hash_to_update.equal?(fallback)

      fallback.each do |key, value|
        next unless hash_to_update[key].nil?

        hash_to_update[key] = value
      end
    end

    # Recursively update nil and missing values in the hash.
    # @param [Hash] hash_to_update the has to update
    # @param [Hash] fallback the hash to get missing values from
    # @example
    #   h = {a: 1, b: { c: nil}}
    #   update = {a: 2, b: {c: 3, d: 4}}
    #   TranslationTools.update_nil!(h, update)
    #   # h => { a:1, b: {c:3, d: 4}}
    # @note This method changes passed `hash` object in place and returns nothing.
    def update_rec!(hash_to_update, fallback)
      return unless fallback.is_a? Hash
      raise ArgumentError, "#{hash_to_update} should be a hash" unless hash_to_update.is_a? Hash

      update_nil!(hash_to_update, fallback)
      fallback.each do |key, value|
        update_rec!(hash_to_update[key], value)
      end
    end

    # Recursively remove keys from the hash that don't appear in fallback hash
    # @param hash_to_clean [Hash{String=>}] the hash to clean
    # @param fallback [Hash{String=>}] the fallback to verify against
    # @note This method changes `hash_to_clean` argument in place and doesn't return anything
    def delete_extra_keys!(hash_to_clean, fallback)
      keys = hash_to_clean.keys - fallback.keys
      keys.each do |key|
        hash_to_clean.delete(key)
      end
      # clean the values of the hash recursively
      hash_to_clean.each do |key, value|
        next unless value.is_a? Hash

        HashUtils.delete_extra_keys!(value, fallback[key])
      end
    end

    # Recursively remove keys from the hash that appear in missing_keys hash
    # @param hash_to_clean [Hash{String=>}] the hash to clean
    # @param missing_keys [Hash{String=>true}] hash of unused keys
    # @example
    #   TranslationTools::HashUtils.delete_unused_keys!
    def delete_unused_keys!(hash_to_clean, missing_keys)
      clean_data = []
      hash_to_clean.each do |key, val|
        if val.is_a? Hash
          values = delete_unused_keys!(val, missing_keys)
          clean_data << [key, values] unless values.empty?
        else
          clean_data << [key, val] unless missing_keys[key]
        end
      end

      clean_data.to_h
    end

    # Cleanup translations that contain english text (eg. when user submits
    #   'user_translations.json' that was copied from provided translation)
    # @example
    #   pt = TranslationTools.load_lang('portuguesePT')
    #   en = TranslationTools.load_lang('english')
    #   clean = TranslationTools::HashUtils.remove_english_translations!(pt, en)
    #   puts JSON.pretty_generate(clean)
    def remove_english_translations!(hash_to_clean, english_translation)
      # Use array to keep order
      clean_hash = []
      hash_to_clean.each do |key, value|
        if value.is_a? Hash
          new_hash = remove_english_translations!(value, english_translation[key])
          clean_hash << [key, new_hash] unless new_hash.empty?
        else
          next if value == english_translation[key]

          clean_hash << [key, value]
        end
      end
      clean_hash.to_h
    end
  end
end
# rubocop:enable Metrics/ModuleLength

if $PROGRAM_NAME == __FILE__
  case ARGV[0]
  when '--copy-to-modelur'
    TranslationTools.copy_to_modelur
  when '--compile'
    TranslationTools.compile
  when '--all'
    TranslationTools.compile
    TranslationTools.copy_to_modelur
  when '--remove_unused_translations'
    TranslationTools.remove_all_unused_translations!
  else
    TranslationTools.usage
  end
end
