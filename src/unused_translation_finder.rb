# frozen_string_literal: true

require_relative 'translation_tools'

# Methods to help us find unused translations
module UnusedTranslationFinder
  module_function

  # Find keys in file
  def find_in_content(content, keys)
    keys.collect do |key|
      [key, content.include?(key)]
    end.to_h
  end

  # def search_unused(glob_pattern="**/src/**/*.{rb,ts,tsx}")
  def search_unused(glob_pattern = '**/*.{rb,ts,tsx}')
    dir = File.join(File.dirname(__FILE__), '..', '..', 'Modelur')
    files = Dir.glob(glob_pattern, base: dir)
    t = TranslationTools.load_lang('english')
    unused_keys = t.keys
    # puts "All keys = #{keys.length}"
    files.each do |file|
      break if unused_keys.empty?

      filepath = File.join(dir, file)
      unused_keys = unused_in_file(filepath, unused_keys)
    end

    dir = File.join(File.dirname(__FILE__), '..', '..', '_react-ui')
    files = Dir.glob(glob_pattern, base: dir)
    files.each do |file|
      break if unused_keys.empty?

      filepath = File.join(dir, file)
      unused_keys = unused_in_file(filepath, unused_keys)
    end

    unused_keys
  end

  def unused_in_file(file, keys)
    content = File.read(file)
    unused_in_file = keys.dup
    keys.each do |key|
      if content.match?(/("#{key}")|('#{key}')|([.]#{key})/)
        # puts "Removing #{key}"
        unused_in_file.delete(key)
      end
    end
    unused_in_file
  end

  def usage
    puts "Usage: ruby #{$PROGRAM_NAME} --find"
  end
end

if $PROGRAM_NAME == __FILE__
  puts ARGV.inspect
  case ARGV[0]
  when '--find'
    puts UnusedTranslationFinder.search_unused
  else
    UnusedTranslationFinder.usage
  end
end
