# Modelur translations

Translations of Modelur user interface in different languages. The language translations are in the JSON format. There is a simple [web interface (i18n.modelur.com)](https://i18n.modelur.com), where users can 
edit and update translations.

## Basic workflow

Compile language files and copy them to Modelur and _react-ui folders

```shell
ruby src/translation_tools.rb --all
```

Compile languages to a language files and create digest

```shell
ruby src/translation_tools.rb --compile
```

Copy translation files to Modelur and _react-ui folders

```shell
ruby src/translation_tools.rb --copy-to-modelur
```

Test compiled language and translation tools

```shell
rake test
```

## Inspect changes

Since ordinary diff will also detect changes in order and formating, it is better to use a tool, that can detect changes in JSON data. 
You can use [json-diff]() or [json diff patch](https://github.com/benjamine/jsondiffpatch) to see actual differences. To inspect the differences use 

```
npx jsondiffpatch file1.json file2.json
```

You can add this to [gitconfig](https://git-scm.com/docs/git-config) file

```
[difftool "jsondiff"]
	cmd = "npx json-diff $LOCAL $REMOTE"
[alias]
	jdiff = difftool -t jsondiff -y
```

To inspect what has changed, simply use 

```
git jdiff
```
