# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../../../src/translation_tools'
require 'json'

class TranslationToolsTest < Minitest::Test
  def setup
    # Do nothing
  end

  def teardown
    # Do nothing
  end

  # rubocop:disable Metrics/MethodLength
  def test_complete_lang
    sl = {
      'a' => 'pes',
      'b' => { short: 'pes', long: 'dolg pes' },
      'c' => nil,
      'd' => { short: 'mačka', long: nil },
      'f' => 'slon'
    }
    an = {
      'a' => 'dog',
      'b' => { short: 'dog', long: 'long dog' },
      'c' => 'cat',
      'd' => { short: 'cat', long: 'long cat' },
      'e' => 'rat'
    }
    expected = {
      'e' => 'rat',
      'b' => { short: 'pes', long: 'dolg pes' },
      'c' => 'cat',
      'd' => { short: 'mačka', long: 'long cat' },
      'a' => 'pes'
    }
    TranslationTools.complete_lang!(sl, an)
    expected.each do |key, value|
      assert_equal value, sl[key]
    end
  end
  # rubocop:enable Metrics/MethodLength

  def test_update_nil
    h = { a: 1, b: nil }
    update = { a: 2, b: 3, c: 4 }
    expected = { a: 1, b: 3, c: 4 }
    TranslationTools::HashUtils.update_nil!(h, update)
    assert_equal expected, h
  end

  def test_language_names
    names = TranslationTools.load_names
    assert_equal 13, names.length
    assert_equal 'Slovenščina', names['slovenian']
  end

  def test_encoding
    slovene = TranslationTools.load_lang('slovenian')
    assert_equal 'č³', slovene['ui_cubicUnit']['feet']
    path = File.join(TranslationTools::LANG_PATH, 'slovenian.json')
    slovene = JSON.parse(File.read(path, encoding: Encoding::UTF_8))
    assert_equal 'č³', slovene['ui_cubicUnit']['feet']
  end

  def test_units
    all = TranslationTools.load_and_complete_all
    exp_keys = %w[apartment office room classroom department hall workshop shop parking_lot na].sort
    all['translation'].each do |key, lang|
      assert_equal exp_keys, lang['ui_unit1']['singular'].keys.sort,
                   "Unit1 for #{key} is not correct"
      assert_equal exp_keys, lang['ui_unit1']['plural'].keys.sort,
                   "Unit1 for #{key} is not correct"
    end
  end

  def test_complete_all
    all = TranslationTools.load_and_complete_all
    assert_equal 13, all['translation'].length
    english = TranslationTools.load_lang('english')
    all['translation'].each do |lang_name, lang|
      assert_equal english.keys.sort, lang.keys.sort,
                   "Keys in #{lang_name} not in english: #{lang.keys - english.keys}
Keys in english not in #{lang_name}: #{english.keys - lang.keys}
"
      assert_same_keys english, lang
    end
    assert_equal all['translation'].keys.sort, all['name'].keys.sort
  end

  def test_compiled
    all = TranslationTools.load_and_complete_all
    all['translation'].each do |lang_name, lang|
      path = File.join(TranslationTools::LANG_PATH, TranslationTools.lang_filename(lang_name))
      saved = JSON.parse(File.read(path, encoding: Encoding::UTF_8))
      assert_equal_unsorted saved, lang
    end
  end

  def test_update_rec
    h = { a: 1, b: 2, c: { d: { e: 1 } } }
    u = { a: 2, b: 2, c: { d: { e: 2, f: 2 } } }
    TranslationTools::HashUtils.update_rec!(h, u)
    assert_equal({ e: 1, f: 2 }, h[:c][:d])
  end

  def test_delete_extra_keys
    h = { a: 1, b: 2, c: { d: 1, e: 2 } }
    fallback = { a: 2, c: { d: 3 } }
    TranslationTools::HashUtils.delete_extra_keys!(h, fallback)
    assert_equal_unsorted({ a: 1, c: { d: 1 } }, h)
  end

  def test_assert_same_keys
    h_1 = { a: 1, b: 2, c: { d: { e: 1 } } }
    h_2 = { a: 0, b: 1, c: { d: { e: 1, f: 2 } } }
    result, message = same_keys? h_1, h_2
    assert_equal '{:e=>1} and {:e=>1, :f=>2} don\'t have same keys at [:c, :d]', message
    refute result
    h_1 = { a: 1, b: 2 }
    h_2 = { a: 0, b: { c: 1 } }
    result, message = same_keys? h_1, h_2
    assert_equal 'value 2 should be a Hash at [:b]', message
    refute result
  end

  def test_digest
    digests = JSON.load_file(TranslationTools::DIGEST_PATH)
    digests.each do |filename, digest|
      path = File.join(TranslationTools::LANG_PATH, filename)
      actual = Digest::SHA256.file(path).hexdigest
      assert_equal digest['SHA256'], actual
    end
  end

  def assert_equal_unsorted(expected, actual, path = [])
    unless expected.is_a? Hash
      refute_nil expected, "values at #{path.join('=>')} should not be nil"
      refute_nil actual, "values at #{path.join('=>')} should not be nil"
      assert_equal expected, actual, "values at #{path.join('=>')} not equal"
      return
    end
    assert_equal expected.keys.sort, actual.keys.sort
    expected.each do |key, value|
      assert_equal_unsorted value, actual[key], path + [key]
    end
  end

  # Assert that the first hash has exactly the same keys as the second
  # rubocop:disable Minitest/AssertWithExpectedArgument
  def assert_same_keys(expected, actual)
    result, message = same_keys?(expected, actual)
    assert result, message
  end
  # rubocop:enable Minitest/AssertWithExpectedArgument

  def same_keys?(hash_1, hash_2, path = [])
    return false, "#{hash_1} is not a Hash at #{path}" unless hash_1.is_a? Hash
    return false, "#{hash_2} is not a Hash at #{path}" unless hash_2.is_a? Hash
    return false, "#{hash_1} and #{hash_2} don't have same keys at #{path}" unless hash_1.keys.sort == hash_2.keys.sort

    all_same_keys?(hash_1, hash_2, path)
  end

  # Check if all values that are Hash have the same keys
  def all_same_keys?(hash_1, hash_2, path)
    hash_2.each do |key, value_2|
      value_1 = hash_1[key]
      next unless value_1.is_a?(Hash) || value_2.is_a?(Hash)
      return false, "value #{value_1} should be a Hash at #{path + [key]}" unless value_1.is_a? Hash
      return false, "value #{value_2} should be a Hash at #{path + [key]}" unless value_2.is_a? Hash

      result, message = same_keys?(value_1, value_2, path + [key])
      return result, message unless result
    end
    [true, nil]
  end
end
