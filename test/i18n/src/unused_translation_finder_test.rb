# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../../../src/unused_translation_finder'
class UnusedTranslationFinderTest < Minitest::Test
  def setup
    # Do nothing
  end

  def teardown
    # Do nothing
  end

  def test_used_in_file
    keys = TranslationTools.load_lang('english').keys
    unused = UnusedTranslationFinder.unused_in_file(
      File.join(__dir__, 'file.test'), keys
    )
    assert_equal 7, (keys - unused).length
  end
end
